# 第一章：量子计算范式

量子计算机在不久的将来展示了对大规模行业带来革命性变革的潜力。量子解决方案（硬件和软件）在其巅峰时期，有可能通过优化计算将人类送上冥王星。根据 Gartner 报告，到 2023 年，20%的组织将为量子计算项目进行预算(*CIO 的量子计算指南*， [`tinyurl.com/yrk4rp2u`](https://tinyurl.com/yrk4rp2u))。该技术承诺通过模拟实现更高的准确性，并提供真实世界的体验。本书深入探讨了量子解决方案在解决现实世界金融问题中的潜在应用。

在本章中，我们将讨论目前处于研究阶段的各种计算范式。还将呈现量子计算的历史。接下来，我们将介绍经典计算机面临的限制，以及这些挑战如何通过量子计算机得以克服。之后，量子计算在塑造下一代商业中的作用将得到定义。

本章后续将介绍量子计算的基础知识。驱动量子计算机的硬件类型将在接下来的章节中描述。我们还将探讨这项技术的潜在商业应用，以及组织如何根据其商业战略来充分利用这些潜力。

本章将涵盖以下主题：

+   量子技术及其相关范式的发展

+   基本量子力学原理及其应用

+   量子计算的商业应用

# 量子技术及其相关范式的发展

计算范式可以定义为多年来取得的重要里程碑。说计算机使人类的生活变得更容易，简直是轻描淡写。日常生活中，我们需要能够分析、模拟和优化复杂问题解决方案的机器。尽管计算机的形态和大小随着时间的推移发生了变化，但它们仍然基于艾伦·图灵和约翰·冯·诺依曼提出的学说进行运作。

在本节中，我们将研究量子技术多年来的发展。我们还将研究该技术在面对某些商业挑战时的局限性。

## 计算范式的演变

图灵向我们展示了计算机能够解决的问题类型，冯·诺依曼构建了可编程计算机，迈克尔·摩尔在半导体领域的开创性工作使计算机变得更加强大。*图 1.1*展示了计算范式多年来的发展，以及它们对人类历史增长的影响：

| 1821 | 机械计算器 | 使人类能够从机械设备过渡到电子设备，在计算精度上有了更好的提升。 |
| --- | --- | --- |
| 1890 | 穿孔卡片系统 | 通过协助美国人口普查，展示了大规模计算的首次应用案例。 |
| 1936 | 图灵机 | 为解决大型计算问题奠定了理论框架。 |
| 1941 | 数字电子计算机 | 首次实现计算机能够在主内存中存储信息。 |
| 1945 | 电子数值积分与计算机 (ENIAC) | 第一台通过重编程执行大规模数值问题的数字计算机。 |
| 1958 | 集成电路 (IC) | 帮助企业级计算向个人计算转型。 |
| 1976 | Cray-1 超级计算机 | 提供了 240 百万次计算，支持大规模科学应用和仿真。 |
| 1997 | 并行计算 | 使用多核 CPU 在有限时间内解决复杂问题，帮助谷歌形成更好的搜索引擎。 |
| 2006 | 云计算 | 使用户能够从远程位置访问大型计算资源。 |
| 2016 | 可重编程量子计算机 | 提供了一个更好的平台，用于解决与经典计算机相比更复杂的仿真或优化问题。 |
| 2017 | 分子信息学 | 利用分子特性进行快速、可扩展的信息存储和处理。 |

图 1.1 – 计算范式的演变

计算技术的发展使人类从农业社会演变为工业社会。计算能力的进步将社会从以物易物推进到建立电子商务平台。*图 1.1* 给出了计算技术如何通过从单纯执行计算的设备到如今的多功能设备，推动社会发展的总结。在下一部分，我们将评估大型企业面临的挑战以及当前数字技术在应对这些挑战时的局限性。

## 企业挑战与技术解决方案

当前的数字技术在实时提供解决方案和洞察方面既有优势也有局限性。众多变量的出现及其日益复杂化可能会影响现实世界中的决策。拥有一种既可靠又精准、同时又快速的技术是至关重要的。对于一种可靠技术堆栈的需求促使全球科学家研究超出人类能力范围的技术。大型企业目前面临的挑战如下：

+   **更快的任务完成**：在当前时代，制造公司致力于实现超大规模生产能力和效率，因此需要建立更快、更可靠的系统。例如，根据人工智能大脑的一项令人兴奋的研究（*如何通过量子退火和遗传算法为电动汽车充电站选址，Quantum Zeitgeist*， [`tinyurl.com/bdep5eze`](https://tinyurl.com/bdep5eze)），在旧金山湾区设置充电站的 50 英里半径内，大约有 8,543,811,434,435,330 种组合是可能的。那么，当可能的组合数如此庞大时，如何优化这种分布呢？量子计算机在不到 3 秒的时间内理论上解决了这个问题。

+   **内容发现**：随着社交媒体网站的兴起，能够分析的内容种类繁多。这些内容以文本和图像的形式存在，大小各异。一个组织需要一台具有超强计算能力的计算机来探索这些内容。这种特殊的计算能力通过并行计算和本地优化机器实现。然而，在这一领域还有许多工作要做，以便从潜在的数据中挖掘出实时的商业洞察。**量子自然语言处理**（**QNLP**）是一种有前景的技术，可以实时解决问题。

+   **降低管理成本**：优化成本始终是一个不错的策略。大规模工厂的自动化为业主提供了一个正确方向的解决方案。大规模自动化伴随着一系列自身的问题，但精确度和实时决策帮助提高了准确性和可靠性。最近，宝马提出了一个挑战，竞争者必须专注于解决基于生产前车辆配置、生产中的材料变形、车辆传感器放置和用于自动化质量评估的机器学习等问题。基于获得的结果，宝马集团研究与新技术数字化汽车副总裁 Peter Lehnert 博士评论道：“我们宝马集团深信，未来的技术，如量子计算，具有使我们的产品更具吸引力和可持续性的潜力”(*在宝马集团量子计算挑战赛中宣布获胜者，AWS 量子计算* *博客*， [`aws.amazon.com/blogs/quantum-computing/winners-announced-in-the-bmw-group-quantum-computing-challenge/`](https://aws.amazon.com/blogs/quantum-computing/winners-announced-in-the-bmw-group-quantum-computing-challenge/))。

+   **远程办公**：2020 年在人类历史中发挥了至关重要的作用。由于新冠疫情的爆发，人们发现自己可以在世界任何地方工作。这促进了管理层和员工对远程办公的需求。由于有些情况需要更高的计算能力，远程办公可能并非在所有情况下都可行。然而，随着大多数技术转向在线并通过虚拟现实和增强现实技术以及更好的连接性提供类似办公室环境的实时工作体验，企业可以克服这一挑战。同时，这也降低了管理层的行政成本。它还有助于进一步降低存储成本，从而帮助公司降低单位成本。

为了更高效和更优化地执行业务任务，商业界开始寻求技术解决方案。当前状态下的数字计算帮助企业通过自动化和增强智能提高了效率。然而，现有的硬件技术尚未能够解决一些复杂任务，这些任务与大量数据和计算内存的限制有关。以下部分将突出数字计算能够解决的各种问题，以及需要超越当前计算范式的其他问题。

## 当前的商业挑战与数字技术的局限性

数字计算机由**集成电路**（**ICs**）提供动力，这项技术在 20 世纪达到了顶峰。根据摩尔定律，推动微芯片的晶体管数量每年将翻倍。2021 年，IBM 宣布其 2 纳米芯片技术能够容纳 500 亿个晶体管，这基本上使得一颗芯片能够装进指甲大小的空间。大量晶体管的存在使得经典计算机能够执行大量计算和复杂的程序，从而帮助更快地解决日常问题。

然而，由于内部泄漏和微型化效应，经典门电路（或门和与门）展示了量子效应。此外，数字计算机传统上无法解决 NP-难问题（*图 1.2*）。用外行话来说，NP-难问题是根据问题的复杂度和变量数量来衡量解决一个问题所需的时间。例如，前文所述的如何从 8,543,811,434,435,330 种充电站位置组合中选择最优路线的问题。尽管经典计算机解决上述问题可能需要数年时间，但理想情况下，量子计算机可以在 3 秒内解决该问题。

![图 1.2 – 基于复杂度水平的 NP-难问题分类](img/B19146_01_002.jpg)

图 1.2 – 基于复杂度水平的 NP-难问题分类

为了更好地理解传统计算机的局限性，假设你必须在有限的预算下挑选 100 只便士股的投资组合，并且假设股票价格是离散的（例如，股市中的最小价格变动）。假设你必须在多项式时间内构建一个投资组合（p = 计算机可以在合理时间内解决的问题），并且假设需要 100 步（n = 输入数量）来获得一个优化的投资组合，换句话说，所需的时间是 n³。从理论上讲，数字计算机将在三小时内解决这个问题。这个问题很容易解决，专家也能轻松验证解决方案，因为我们处理的是同一类股票。因此，我们可以自信地说，p 类问题很容易验证和解决。现在，假设同样的问题发生变化（在有限的时间内优化属于不同风险类别的 100 只股票的投资组合），这将需要约 300 万亿年才能解决，因为尽管解决方案可以在多项式时间（n）内验证，但它是以指数时间（NP）获得的。这个问题被归类为 NP 问题。打个比方，想象一个数独或井字游戏问题：这是一个 NP 问题，尽管它的解决方案很难获得（需要指数时间），但可以在多项式时间内轻松验证。

继续前面的讨论，以下是被认为数字计算机难以解决的四种 NP 问题类型：

+   **模拟**：计算模拟是将自然世界或物理系统建模为虚拟场景，以便提前了解其结果和影响。例如，在 2008 年次贷危机之后，金融机构必须对其基础资产和交叉持股进行压力测试，以预测下一次金融崩溃可能发生的场景。据估计，对于一个简单的 20-30 家机构组成的网络，这些机构涉及股票、衍生品、固定收益证券，并彼此有风险暴露，评估金融崩溃的概率将需要 137 亿年时间，由数字计算机计算得出。这是估算的时间，用于运行一个本质上是确定性的模拟问题，暗示着在 p 时间内通过 n 步解决问题的复杂性，这种问题不能使用当前的数字技术解决，因此需要一个更先进的系统来提供更快的处理速度。

+   **优化**：优化是指改进现有算法的效率，以降低时间复杂度。假设你需要构建一个包含 1,000 只股票的投资组合，涉及 10 个行业。你的客户，一家国际对冲基金，必须根据市场条件生成多个情景，从而寻找一个有效前沿。这些情景需要实时更新，并根据为投资组合定义的风险容忍限额进行调整。传统计算机可以使用并行计算来解决这个难题，但这可能不是最具成本效益和时间效率的策略。这个问题凸显了实时解决该难题的高效计算机的需求。

+   **模式识别**：模式识别方法利用基础数据，通过机器学习算法发现隐藏的模式和趋势。然而，近年来 GPU 及相关技术的进步使程序员在理解和揭示给定数据中的隐藏模式方面取得了相当大的成功。然而，在金融欺诈领域，由于人类行为的复杂性，机器学习算法很难理解其中的模式。理论上，一台能够实时理解数据的计算机可以更成功地解码金融欺诈的模式。

+   **密码学**：在这个互联的世界中，为客户提供安全的在线交易通道是 21 世纪银行的首要任务。在全球范围内，银行使用基于线性因式分解的**里维斯特、沙米尔和阿德尔曼**（**RSA**）技术。最近计算能力的发展表明，这种加密技术可以通过量子计算机轻松破解。

总结来说，可以简单地说，在当前技术的局限性下，探索新的计算范式已经到了时机，这些新范式能够帮助解决商业界面临的问题，并推动行业创新与创造力。

# 基本量子力学原理及其应用

量子计算机使用原理和理论（如量子场论和群论）来描述量子力学现象。量子力学原理，如叠加、去相干和纠缠，已被应用于构建处理和传递信息的处理器，其速度呈指数级增长。以下部分展示了量子计算机的演变过程，并简要描述了量子力学原理。

## 量子计算技术在下一代企业中的新兴角色

长期以来，由于数字计算机在规模经济上的进步，抑制了其他计算范式的发展。摩尔定律（*图 1**.3*）预测了微处理器的指数增长和进步。然而，几十年来积累的大量数据使计算能力、存储和通信受到了限制。为了克服当前架构的局限性，我们必须克服有限内存、自编程计算机、大数分解和更快微处理器等挑战。

![图 1.3 – 根据摩尔定律的晶体管数量增长](img/B19146_01_003.jpg)

图 1.3 – 根据摩尔定律的晶体管数量增长

由于数字计算机的基本原理和假设存在当前的局限性，需要出现新的计算范式。为了应对与气候、过程自动化、工业机械化和自主系统等多个领域相关的问题，需要克服当前的挑战。量子计算、分子计算、自然启发的算法以及人机协同互动（《计算机特刊 2016 年 9 月研究“下一代计算范式”》，IEEE 计算机学会，[`tinyurl.com/4b5wjepk`](https://tinyurl.com/4b5wjepk))是当前为克服上述挑战而进行的创新和研究方向。*图 1**.4*描绘了量子计算范式从理论到实际应用的历程和影响：

| **年份** | **现象** | **影响** |
| --- | --- | --- |
| 1905 | 阿尔伯特·爱因斯坦发现了光电效应，并且光子被发现。 | 为发现原子粒子中的量子行为奠定了基础。 |
| 1924 至 1927 年 | 马克斯·玻恩创造了“量子力学”这一术语，海森堡、玻恩和乔丹发现了矩阵力学。 | 发现了量子力学原理，这些原理被用来生产量子处理器。 |
| 1935 | 埃尔温·薛定谔构思并写下了他的思想实验——薛定谔的猫。 | 发现了量子纠缠原理。 |
| 1976 | 罗曼·斯坦尼斯瓦夫·英加登提出了量子信息理论。 | 量子信息科学作为一门学科被制定，奠定了量子算法的基础。 |
| 1981 | 理查德·费曼提出量子计算机有潜力模拟物理现象。 | 量子力学的实际应用被用来开发工作量子计算机。 |
| 1994 | 发现了肖尔算法用于整数分解。 | 为量子密钥分发后的密码学奠定了基础。 |
| 1996 | 发现了格罗弗算法。 | 为以数据库形式存储信息铺平了道路。 |
| 2011 | D-Wave 提供了首个使用量子退火技术的量子计算解决方案。 | 开启了量子计算在商业用途上的可能性。 |
| 2019 | Google 宣称量子霸权。 | 展示了量子霸权的一个应用案例，有助于更好的加密。 |
| 2021 | IBM 揭晓了首个 127 量子比特的量子计算机，名为 Eagle。 | 加速了复杂的 NP 困难问题的处理。 |

图 1.4 – 从量子力学到量子计算的历程

如图所示（*图 1.4*），从发展历程的角度来看，量子技术正快速进展，解决诸如精准仿真、高效优化和正确模式识别等问题。一旦研究人员能够克服当前用户面临的相关问题，并实现量子技术来解决日常问题，便能看到量子技术在整个行业中的广泛应用将如何解决大规模问题。

下一节将介绍一些在构建和操作量子计算机中常用的量子力学术语和原理。

## 从量子力学到量子计算

解读涉及量子计算的量子力学原理对外行人来说是一项艰巨的任务。本节将用通俗易懂的语言描述每一个量子力学公设，并解释它是如何参与量子计算机制的。

| 公设 | 定义 | 用法 | 深入阅读 |
| --- | --- | --- | --- |
| 量子比特（Qubits） | 量子比特是量子信息的基本单位，存储在一个二态设备中，以 0 和 1 的形式编码信息。 | 有助于加速复杂过程（如仿真和优化）中的信息处理。 | 什么是量子比特？（quantuminspire.com） |
| 量子态 | 量子态是原子粒子的位置和属性（变化与自旋）的值，这些值可以通过自然获得或通过创造物理环境（例如激光和热量）来诱导。 | 用于在受控环境中通过量子比特处理和转换信息。 | 叠加与纠缠（quantuminspire.com） |
| 量子叠加 | 量子叠加指的是量子叠加现象，告诉我们量子叠加可以被视为量子态的线性组合。 | 这一特性使得系统很难解密量子通信，从而提供了一种更安全的信息传输方式。 | 叠加与纠缠（quantuminspire.com） |
| 量子纠缠 | 量子纠缠指的是两个粒子处于相同量子态并且它们之间存在关联的现象。 | 通过越来越多的量子比特（qubits），使得系统能够以指数速度进行计算。 | 叠加与纠缠（quantuminspire.com） |
| 量子测量 | 一组数学算子，用于理解和测量可以从量子比特中恢复和处理的信息量。 | 有助于理解量子力学的复杂性。 | 量子测量将信息分为三部分 - Physics World. |
| 量子干涉 | 它指的是原子粒子能够像波粒一样表现的能力，从而导致信息或量子比特状态的坍缩，进而引发量子相干或去相干。 | 它衡量量子计算机准确计算和承载存储在其中的信息的能力。 | 什么是量子力学？量子计算研究所（uwaterloo.ca） |
| 无克隆定理 | “**无克隆定理**”是量子力学的一个结果，禁止创建任意未知量子状态的相同副本。 | 无克隆定理是量子密码学中的一个重要组成部分，因为它禁止窃听者创建传输的量子密码学密钥的副本。 | 无克隆定理 - Quantiki |

图 1.5 – 量子计算术语表

*图 1.5*中提到的假设使计算机科学家能够从经典计算机过渡到量子计算机。正如我们在后续章节中将看到的那样，量子干涉和无克隆定理等假设使量子技术走到前沿，并为实现更快、更高效、更准确的计算能力奠定了基础。接下来的部分将探讨推动量子计算范式创新的技术。

## 量子创新方法

在当前形式下，量子计算依赖于多种技术来扩展其影响力。量子计算机要完全实现其商业潜力还需要数年时间。然而，当它们与经典计算机以混合模式工作时，预计它们会比单独运行时产生更好的结果。让我们来看看推动它们运行的技术：

+   **超导**：这项技术利用了量子物理中的叠加特性。信息通过两股方向相反的带电电子电流在超导体中循环流动，然后在纠缠的过程中交换存储在量子比特中的信息。这项技术需要量子计算机在极低的温度下运行。

+   **困陷离子**：离子是带电的原子（如 Ca+或 Br+）。假设一段信息编码在这个带电的原子上。通过发射能量脉冲，原子从状态 0 被转移到状态 1。这个带电的原子将携带信息，并在激光的帮助下解码。这些离子被困在电场中。编码的信息通过光子单元进行解释，然后通过光纤传递。

+   **光子学**：这项技术利用光子在量子态中承载信息。通过当前的硅芯片，可以控制光子的行为，并将信息通过电路传输。由于与现有基础设施和芯片制造能力的兼容性，它有望取得巨大的成功。

+   **量子点**：量子点是由硅、镉等元素构成的小型半导体纳米晶体，其大小范围为 2 到 10 纳米。量子比特的物理实现涉及通过电荷量子比特在电容状态下交换信息。由于其有利的条件，光子学更少出现错误。

+   **冷原子**：冷原子使用与困束离子类似的方法，原子被冷却至低于 1 mK，然后用作信息通道，将信息反射回来。激光被编程以控制冷原子的量子行为，并利用它们来传输数据。

为了理解每项技术所取得的里程碑，我们将借助迪文琴佐标准。2000 年，David DiVincenzo 提出了一份量子计算机实验特性的愿望清单。自那时以来，迪文琴佐标准已成为物理学家和工程师构建量子计算机的主要指南（Alvaro Ballon，*超导量子比特的量子计算*，PennyLane，[`tinyurl.com/4pvpzj6a`](https://tinyurl.com/4pvpzj6a)）。这些标准如下：

+   **良好表征且可扩展的量子比特**：自然界中看到的许多量子系统并非量子比特；因此，我们必须开发一种方法使它们表现得像量子比特。此外，我们还需要整合多个这样的系统。

+   **量子比特初始化**：我们必须能够在可接受的误差范围内复制相同的状态。

+   **延长的相干时间**：量子比特在与周围环境长时间交互后会失去其量子特性。我们希望它们足够耐用，以便支持量子过程的进行。

+   **通用门集**：必须对量子比特执行任意操作。为此，我们需要单量子比特门和双量子比特门。

+   **单个量子比特的量化**：为了确定量子计算的结果，必须精确测量预定量子比特集的最终状态。

*图 1.6* 有助于根据迪文琴佐标准评估每种量子技术的前景和缺点：

|  | **超导** | **困束离子** | **光子学** | **量子** **点** | **冷原子** |
| --- | --- | --- | --- | --- | --- |
| 良好表征且可扩展的量子比特 | 已实现 | 已实现 | 已实现 | 已实现 | 已实现 |
| 量子比特初始化 | 已实现 | 已实现 | 已实现 | 已实现 | 已实现 |
| 延长的相干时间 | 99.6% | 99.9% | 99.9% | 99% | 99% |
| 通用门集 | 10-50 ns | 1-50 us | 1 ns | 1-10 ns | 100 ns |
| 单个量子比特的量化 | 已实现 | 已实现 | 已实现 | 已实现 | 已实现 |

图 1.6 – 迪文琴佐标准

在各项参数上，超导和捕获离子等技术在克服量子技术挑战方面显示出最大的潜力。虽然像 IBM 和谷歌这样的超级巨头正押注于这类技术来开发它们的量子计算机，但包括 IQM 和 Rigetti 在内的新时代初创公司则在探索与当前基础设施更兼容的其他技术。

在接下来的章节中，我们将详细介绍与量子计算生态系统相关的应用和技术。

## 量子计算价值链

量子计算技术仍处于起步阶段。如果从技术角度类比，1975 年，大多数投资者都在投资硬件公司，如 IBM、惠普，后来是苹果，以确保人们能够从大型主机迁移到个人计算机。一旦从硬件中获得了价值，他们开始关注软件，公司如微软因此崭露头角。根据 BCG 发布的报告，80%的资金正流向硬件公司，如 IonQ、ColdQuanta 和 Pascal。需要克服的关键工程挑战包括可扩展性、稳定性和运营。

多家公司和初创公司正在投资量子计算。美国（20 亿美元）、中国（10 亿美元）、加拿大（10 亿美元）、英国（10 亿英镑）、德国（20 亿欧元）、法国（18 亿欧元）、俄罗斯（7.9 亿美元）和日本（2.7 亿美元）已承诺巨额资金以实现量子霸权。有人推测，包括量子传感器、量子通信和量子互联网在内的量子解决方案需要大量投资，以帮助各国实现量子霸权。麦肯锡估计量子计算初创公司数量为 200 家。此外，根据 PitchBook（市场数据分析公司）的数据，全球在量子技术领域的投资已从 2015 年的 9350 万美元增加到 2021 年的 10.2 亿美元。最近吸引了大量投资的一些知名初创公司包括 Arqit、Quantum eMotion、Quantinuum、Rigetti、D-Wave 和 IonQ。

*图 1.7* 显示了量子技术在不同领域的潜在应用，依据量子计算机解决的各种问题类型：

![图 1.7 – 量子计算应用](img/B19146_01_007.jpg)

图 1.7 – 量子计算应用

以下技术正在帮助公司在量子领域为最终用户创造价值链：

+   **量子计算**：量子计算指的是利用量子力学原理开发软件和硬件技术。

+   **量子密钥分发**（**QKD**）：QKD，或称量子加密，提供了一种安全的方式，供银行和其他机构交换加密密钥。它利用量子力学原理来保护通信通道。

+   **量子软件和量子云**：量子软件，或如 Qiskit 这样的编程语言，为终端用户提供了与系统硬件接口并执行复杂计算操作的媒介，包括仿真、优化和模式识别。

![图 1.8 – 量子技术](img/B19146_01_008.jpg)

图 1.8 – 量子技术

+   **后量子加密**：国家投资数十亿美元的关键研究领域之一是当前加密软件可能会受到量子算法的威胁。它们需要能够进一步保障这些渠道安全的算法。

+   **量子传感器和原子钟**：这些术语指的是激光和困陷离子技术的发展，用于控制分子原子行为。这促使研究人员开发出使用量子传感器等下一代技术，在早期检测自然灾害（包括海啸和地震）的应用场景。

+   **量子材料**：量子材料指的是一类世界级的技术，用于捕捉和操控元素的量子特性，以便在工业中应用。

+   **量子存储器和其他量子组件**：这些设备通过光子以量子位（qubit）形式携带信息。它是复杂的技术，仍在开发中，预计能够克服当前限制所定义的存储障碍。

如*图 1.8*所示，量子计算生态系统广阔。它涵盖了多个方面，如量子材料、存储器和传感器，帮助用户更有效地收集和分析数据。

在接下来的章节中，我们将探讨推动量子技术革命的公司。

# 量子计算的商业应用

尽管量子技术仍处于起步阶段，尚未实现商业应用，但它具有巨大的潜力。在不久的将来，量子计算机将能够与经典计算机结合，加速解决复杂问题的进程。在本节中，您将了解这种令人惊叹的技术的商业应用。

## 量子计算领域在整个价值链中的全球参与者

根据麦肯锡报告（量子计算的资金持续强劲，但人才缺口引发担忧，[`tinyurl.com/5d826t55`](https://tinyurl.com/5d826t55)），量子技术已吸引了来自各国政府和资助机构的 7 亿美元投资。这项技术所展现的潜力促使业内资助各大高校和实验室的持续研究。D-Wave 是首家在 1999 年通过量子退火技术开创量子计算解决方案的公司。此后，IBM 等公司建立了强大的研究人员和终端用户社区，推动量子计算机的使用。以下是一些在量子技术领域做出开创性工作的公司：

+   **IonQ**（纳斯达克：IONQ）：IonQ 由 Christopher Monroe 和 Jungsang Kim 于 2015 年创立，至今已获得 4.32 亿美元的融资。IonQ 基于离子陷阱技术构建量子计算机，并将其作为**平台即服务**（**PaaS**）提供给服务提供商。

+   **Rigetti**（纳斯达克：RGTI）：Rigetti Computing 由 Chad Rigetti 于 2013 年创立。目前已获得 5 亿美元的融资。Rigetti 开发了一台基于超导技术的量子计算机。

+   **Quantum Computing Inc.**：Quantum Computing Inc.专注于为最终用户提供软件和硬件解决方案。它还专注于为公司开发商业应用案例，从而展示量子计算在不久的未来的潜力。

+   **Archer**（ASX：AXE）：Archer 是一家澳大利亚公司，正在开展室温量子计算机的研发。由 Mohammad Choucair 博士创立，旨在生产能够广泛应用的量子计算机。

+   **D-Wave**（即将通过 SPAC 合并推出）：D-Wave 因推出全球首台商用量子计算机而闻名。它采用量子退火技术为最终用户开发量子解决方案。D-Wave 提供一项有限但强大的技术，其量子计算机拥有 5000 个量子比特，具有广泛的潜在商业应用。

+   **Quantinuum**：Quantinuum 的成立源于剑桥量子与霍尼韦尔量子解决方案的合并。剑桥量子的主要焦点是开发量子计算机的操作系统和软件，而霍尼韦尔则主要专注于利用离子陷阱技术开发量子计算机。

量子计算领域的全球玩家包括 IBM、微软和谷歌等巨头，以及 Rigetti、IQM 和 Quantinuum 等资金充足的初创公司。这些公司已经在不同类型的技术（硬件和软件）上进行了投资，以推动这一技术领域的研究。

在接下来的部分中，我们将评估不同科技巨头为实现全面量子霸权所提供的路线图。

## 构建量子计算战略实施路线图

构建量子优势以解决实时业务问题是许多量子领域公司最终的目标。这项技术被认为有助于公司解决大规模问题。最近，宝马公司委托了一项价值百万美元的挑战，旨在通过**AWS Amazon Braket**平台找到解决其库存调度问题的方法。在*图 1.9*中，你可以绘制出可能导致量子霸权时代的路径，并看到我们如何利用量子计算解决更多问题：

![图 1.9 – 量子计算时代](img/B19146_01_009.jpg)

图 1.9 – 量子计算时代

广义上，量子时代可以分为三个部分：

+   **噪声中等规模量子**（**NISQ**）：这一时代的特点是拥有较少数量的优质量子比特（<50 到 100）来解决现实问题。*噪声*一词指的是量子比特由于干扰而失去其量子态的倾向。预计目前的技术设置将在 2030 年之前摆脱 NISQ 时代。

+   **广泛的量子优势**：IonQ 将广泛的量子优势定义为量子计算机进入开发者和终端用户的时代，能够解决现实生活中的问题。根据行业从业者达成的共识，72 量子比特的系统将开始帮助行业解决商业级问题。因此，未来将有可能通过展示高级应用编程和人机界面（HMI）功能来访问该平台。

+   **全规模容错**：这一时代指的是已经实现 99.5%两量子比特门保真度的大规模量子计算机。到 2040 年，预计现有的努力将有助于解决**退相干**（由于大量量子比特而导致的信息泄漏）问题，并使组织能够充分利用这一令人惊叹的技术。

量子技术在近期将以混合计算的形式提供给终端用户。为了充分利用现有量子计算机的潜力，像 D-Wave 和 Rigetti 这样的公司已经开始通过微处理器提供经典计算与量子计算之间的接口。经典组件负责与终端用户的通信，而量子微处理器则用于解决 NP-hard 问题。量子技术通过量子退火器和通用量子计算机，利用超导和离子阱等技术，将能够在不久的将来充分发挥其潜力。

在下一部分，我们将看看建设量子技术及其生态系统需要什么样的人才。

## 为量子跃迁建设人才队伍

量子技术需要多种类型的人才来发挥其真正的潜力。整个技术栈可以分为硬件、软件和相关技术。目前，该技术需要科学研究和技术实施专家。根据福布斯编制的一份调查报告，毕业生必须拥有 STEM（科学、技术、工程和数学）学科的本科学位，才能理解量子计算机的基本工作原理。具备研究导向的思维方式对于进一步研究量子计算机的发展至关重要。为了实现与计算机硬件发展相关的科学突破，研究人员必须深入理解退火、超导和离子阱等底层技术。这些技术处于能够通过知识丰富的劳动力实现科学突破的前沿。

除了构建量子计算机，操作量子计算机同样具有挑战性。当前软件开发的重点是编写能够与量子计算机内存核心接口的低级程序。IBM 和谷歌等公司已经开发了基于 Python 的**软件开发工具包**（**SDKs**），如 Qiskit 和 Cirq。IBM 暑期学校等项目是开发人员熟悉与量子内存处理器接口的方法论的良好起点。由于当前量子领域技术的限制，许多重点放在开发混合计算机上。软件开发人员需要了解云计算才能操作量子计算机。大多数量子计算机位于大房间内，温度低于冰点，并且可以通过云计算远程访问。为量子计算机编写的算法也被用来提升现有机器学习算法的性能。

量子解决方案还包括辅助技术，这使得量子技术成为一个令人兴奋的领域。量子传感器、退火机和互联网是量子力学的潜在应用。此外，量子算法在解决与金融、供应链、医疗健康和密码学相关的问题方面也显示出了潜力。*图 1.8*总结了与进入量子技术领域相关的能力和资格的讨论：

|  | **研究领域** | **应用** | **潜力** **资格** |
| --- | --- | --- | --- |
| 硬件 | 量子力学、理论物理、应用物理 | 超导离子阱、量子点 | 量子物理博士、硕士 |
| 软件 | 量子信息科学 | 量子算法、量子机器学习 | 软件开发、计算机科学硕士 |
| 量子商业技术 | 优化、仿真与密码学 | 金融、供应链、医疗健康 | 商业倡导者、领域专家 |

图 1.10 – 资格列表

从*图 1.10*中可以观察到，潜在的量子技术候选人需要具备一定的 STEM 背景。研究能力以及学习、忘记、再学习和应用新概念的能力，是在这个动态领域中生存的必备条件。由于这是一个以研究为导向的领域，企业更倾向于招收相关领域的博士候选人。然而，能够为混合计算机编写代码以更快、更准确地解决问题的软件工程师需求也非常大。

# 总结

计算范式，如计算器、模拟计算机和数字计算机，随着时间的推移不断发展，帮助人类在技术发展上取得迅速进展，达到新的知识前沿。Jon von Neumann、Alan Turing 和 Graham Moore 的贡献在实现更强大计算能力方面发挥了巨大作用。

当前的商业环境催生了基于数据做出更快速、更精准决策的需求。因此，需要更快速、优化的计算机来处理海量数据。

数字计算机无法解决 NP 难问题，包括模拟、优化和模式匹配问题，从而强调了需要新型计算技术来进行更快速、更精准的计算。

新兴的计算范式，如量子计算和分子计算，承诺能够更有效、更高效地解决大规模问题，如投资组合优化、蛋白质折叠和供应链路线优化。

量子计算基于量子力学的基本原理，如量子比特（qubits）、量子态、叠加、干涉、纠缠和量子测量。

目前的量子硬件和微处理器基于超导、捕获离子、退火、冷原子和模拟器等技术。

量子计算的价值链基于通过量子传感器、量子通信和量子互联网等量子解决方案和技术所实现的创新。

在量子计算领域的全球参与者包括 IBM、微软、谷歌等巨头，以及资金充足的初创公司，如 Rigetti、IQM 和 Quantinuum。

将商业战略与量子计算对接，涉及基于量子计算的不同阶段（如 NISQ、广泛的量子优势和全规模容错）的战略路线图开发。

未来的量子工作力量需要在硬件、软件及相关量子技术的发展方面做好三方面的工作。
